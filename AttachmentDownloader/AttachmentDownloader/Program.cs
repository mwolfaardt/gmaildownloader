﻿namespace AttachmentDownloader
{
	using Google.Apis.Auth.OAuth2;
	using Google.Apis.Gmail.v1;
	using Google.Apis.Gmail.v1.Data;
	using Google.Apis.Services;
	using System;
	using System.IO;
	using System.Linq;
	using System.Threading.Tasks;

	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				new Program().Download().Wait();
			}
			catch (AggregateException ex)
			{
				foreach (var e in ex.InnerExceptions)
				{
					Console.WriteLine("Exception: " + e.Message);
				}
			}
			Console.WriteLine("Press any key to continue");
			Console.ReadKey();
		}

		public async Task Download()
		{
			Authenticate authenticate = new Authenticate();
			using (var stream = new FileStream("client_secrets_desktop.json", FileMode.Open, FileAccess.Read))
			{
				UserCredential credentials = await authenticate.AuthenticateUserAsync(stream);
				GmailService gmailService = new GmailService(new BaseClientService.Initializer() { HttpClientInitializer = credentials, ApplicationName = "Gmail Test" });
				Download download = new Download("c:\\temp");
				foreach (Message message in download.GetMessages(gmailService))
				{
					if (!string.IsNullOrEmpty(message.Id))
					{
						UsersResource.MessagesResource.GetRequest getRequest = gmailService.Users.Messages.Get("me", message.Id);
						getRequest.Format = UsersResource.MessagesResource.GetRequest.FormatEnum.Full;
						Message requestMessage = getRequest.Execute();
						string from = requestMessage.Payload.Headers.Where(h => h.Name == "From").Select(h => h.Value).FirstOrDefault();
						if (!string.IsNullOrEmpty(from))
						{
							from = from.Replace("<", string.Empty).Replace(">", string.Empty).Replace(":", string.Empty);
							download.DownloadMessageAttachments(gmailService, "me", requestMessage, from);
						}
					}
					else
					{
						Console.WriteLine("No ID Found");
					}
				};
			}
		}
	}
}


