﻿namespace AttachmentDownloader
{
  using AttachmentDownloader.Interfaces.Providers;
  using Google.Apis.Auth.OAuth2;
  using Google.Apis.Gmail.v1;
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;
  using System.Threading;
  using System.Threading.Tasks;

  public class Authenticate : IGoogle
  {
    public Task<Google.Apis.Auth.OAuth2.UserCredential> AuthenticateUserAsync(System.IO.FileStream JsonFile)
    {
      Task<UserCredential> userCredential = GoogleWebAuthorizationBroker.AuthorizeAsync(GoogleClientSecrets.Load(JsonFile).Secrets, new[] { GmailService.Scope.GmailReadonly }, "user", System.Threading.CancellationToken.None);
      return userCredential;
    }

    public UserCredential AuthenticateUser(System.IO.FileStream JsonFile)
    {
      throw new NotImplementedException();
    }
  }
}
