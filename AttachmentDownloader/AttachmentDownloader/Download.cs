﻿namespace AttachmentDownloader
{
	using AttachmentDownloader.Interfaces;
	using Google.Apis.Gmail.v1;
	using Google.Apis.Gmail.v1.Data;
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	public class Download : IAction
	{
		private FileHandler fileHandler;

		public Download(string DownloadPath)
		{
			fileHandler = new FileHandler();
			fileHandler.DownloadPath = DownloadPath;
		}

		public void DownloadMessageAttachments(Google.Apis.Gmail.v1.GmailService gmailService, string userId, Google.Apis.Gmail.v1.Data.Message message, string outputDir)
		{
			try
			{
				string dateString = message.Payload.Headers.Where(h => h.Name == "Date").Select(h => h.Value).FirstOrDefault();
				DateTime? CreatedOn = null;
				DateTime ParseDateTime;
				if (!String.IsNullOrEmpty(dateString))
				{
					if (DateTime.TryParse(dateString, out ParseDateTime))
					{
						CreatedOn = DateTime.Parse(dateString);
					}
				}
				IList<MessagePart> parts = message.Payload.Parts;
				foreach (MessagePart part in parts)
				{
					if (!String.IsNullOrEmpty(part.Filename))
					{
						String attId = part.Body.AttachmentId;
						if (string.IsNullOrEmpty(message.Id))
						{
							Console.WriteLine(string.Format("Message {0} ID is null", message.Raw));
							return;
						}
						MessagePartBody attachPart = gmailService.Users.Messages.Attachments.Get(userId, message.Id, attId).Execute();

						// Converting from RFC 4648 base64-encoding
						// see http://en.wikipedia.org/wiki/Base64#Implementations_and_history
						String attachData = attachPart.Data.Replace('-', '+');
						attachData = attachData.Replace('_', '/');

						byte[] data = Convert.FromBase64String(attachData);

						if (fileHandler.AllowedExtentions.Contains(part.Filename.Substring(part.Filename.LastIndexOf('.'))))
						{
							outputDir = Path.Combine(fileHandler.DownloadPath, outputDir);
							if (!Directory.Exists(outputDir))
							{
								Directory.CreateDirectory(outputDir);
							}

							string fileName = string.Concat(CreatedOn.HasValue ? CreatedOn.Value.ToString("yyyy-MM-dd - ") : string.Empty, part.Filename);
							if (File.Exists(Path.Combine(outputDir, fileName)))
							{
								string filePrefix = fileName.Substring(0, 18);
								string[] splits = filePrefix.Split('-');
								// TOOD: get better file update

								int countPlaceHolder = 0;
								if (splits.Count() >= 4)
								{
									int.TryParse(splits[3], out countPlaceHolder);
									if (countPlaceHolder != 0)
									{
										countPlaceHolder = countPlaceHolder + 1;
									}
									else
									{
										countPlaceHolder = 01;
									}
								}
								fileName = string.Join(" - ", CreatedOn.HasValue ? CreatedOn.Value.ToString("yyyy-MM-dd") : string.Empty, countPlaceHolder <= 9 ? countPlaceHolder.ToString("00") : countPlaceHolder.ToString(), part.Filename);
							}
							File.WriteAllBytes(Path.Combine(outputDir, fileName), data);
							// set the created on and modified on dates
							if (File.Exists(Path.Combine(outputDir, fileName)))
							{
								if (CreatedOn.HasValue)
								{
									File.SetCreationTime(Path.Combine(outputDir, fileName), new DateTime(CreatedOn.Value.Year, CreatedOn.Value.Month, CreatedOn.Value.Day, CreatedOn.Value.Hour, CreatedOn.Value.Minute, CreatedOn.Value.Second));
									File.SetLastWriteTime(Path.Combine(outputDir, fileName), new DateTime(CreatedOn.Value.Year, CreatedOn.Value.Month, CreatedOn.Value.Day, CreatedOn.Value.Hour, CreatedOn.Value.Minute, CreatedOn.Value.Second));
									File.SetLastAccessTime(Path.Combine(outputDir, fileName), new DateTime(CreatedOn.Value.Year, CreatedOn.Value.Month, CreatedOn.Value.Day, CreatedOn.Value.Hour, CreatedOn.Value.Minute, CreatedOn.Value.Second));
								}
							}
						}
					}
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public Message GetMessage(Google.Apis.Gmail.v1.GmailService gmailService, string messageId)
		{
			throw new NotImplementedException();
		}

		public List<Message> GetMessages(Google.Apis.Gmail.v1.GmailService gmailService)
		{
			List<Message> messages = new List<Message>();
			UsersResource.MessagesResource.ListRequest request = gmailService.Users.Messages.List("me");
			string query = "is:inbox has:attachment";
			request.Q = query;
			do
			{
				ListMessagesResponse response = request.Execute();
				messages.AddRange(response.Messages);
				request.PageToken = response.NextPageToken;
			}
			while (!string.IsNullOrEmpty(request.PageToken));

			return messages;
		}
	}
}
