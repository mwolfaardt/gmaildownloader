﻿namespace AttachmentDownloader
{
  using AttachmentDownloader.Interfaces;
  using System;
  using System.Collections.Generic;
  using System.IO;
  using System.Linq;
  using System.Text;
  using System.Threading.Tasks;

  public class FileHandler : IFileHandler
  {
    public string DownloadPath { get; set; }

    public string[] AllowedExtentions
    {
      get
      {
        return new string[] { ".pdf" };
      }
    }

    public string FileNameFormater(string file)
    {
      return Path.GetInvalidFileNameChars().Aggregate(file, (current, c) => current.Replace(c.ToString(), string.Empty));
    }
  }
}
