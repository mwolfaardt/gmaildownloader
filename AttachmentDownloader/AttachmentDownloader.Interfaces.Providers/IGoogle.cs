﻿
namespace AttachmentDownloader.Interfaces.Providers
{
  using Google.Apis.Auth.OAuth2;
  using System.IO;
  using System.Threading.Tasks;

  public interface IGoogle
  {
    UserCredential AuthenticateUser(FileStream JsonFile);

    Task<Google.Apis.Auth.OAuth2.UserCredential> AuthenticateUserAsync(FileStream JsonFile);
  }
}
