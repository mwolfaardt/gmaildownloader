﻿namespace AttachmentDownloader.Interfaces
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;

  public interface IConfiguration
  {
    string JsonPath { get; set; }
  }
}
