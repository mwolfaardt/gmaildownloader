﻿namespace AttachmentDownloader.Interfaces
{
  using Google.Apis.Gmail.v1;
  using Google.Apis.Gmail.v1.Data;
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;
  using System.Threading.Tasks;

  public interface IAction
  {
    void DownloadMessageAttachments(GmailService gmailService, String userId, Message message, String outputDir);
    Message GetMessage(GmailService gmailService, string messageId);
    List<Message> GetMessages(GmailService gmailService);
  }
}
