﻿namespace AttachmentDownloader.Interfaces
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;

  public interface IFileHandler
  {
    /// <summary>
    /// Path to download file attachments
    /// </summary>
    string DownloadPath { get; set; }
    
    /// <summary>
    /// Allowed files extentions to download
    /// </summary>
    string[] AllowedExtentions { get; }

    /// <summary>
    /// Formats file name
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    string FileNameFormater(string file);
  }
}
